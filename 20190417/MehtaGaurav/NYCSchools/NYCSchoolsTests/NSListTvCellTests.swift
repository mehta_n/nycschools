//
//  NSListTvCellTests.swift
//  NYCSchoolsTests
//
//  Created by Gaurav Mehta on 16/04/19.
//  Copyright © 2019 Gaurav Mehta. All rights reserved.
//


import XCTest
import Foundation

@testable import NYCSchools

class NSListTvCellTests: XCTestCase {
    var listTvCell = NSListTvCell()
    var schoolList : NSSchoolList?

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAwakeFromNib(){
        listTvCell.awakeFromNib()
    }
    
    func testSetSelected(){
        listTvCell.setSelected(true, animated: true)
    }
}
