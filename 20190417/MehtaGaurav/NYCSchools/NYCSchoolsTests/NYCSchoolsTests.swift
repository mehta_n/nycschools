//
//  NYCSchoolsTests.swift
//  NYCSchoolsTests
//
//  Created by Gaurav Mehta on 16/04/19.
//  Copyright © 2019 Gaurav Mehta. All rights reserved.
//

import XCTest
@testable import NYCSchools

class NYCSchoolsTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testAlert() {
        let alertController = NSAlertViewController(title: "Test", message: "test", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
        })
        alertController.addAction(ok)
        alertController.show()
    }
}
