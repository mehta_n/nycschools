//
//  NSHighSchoolListTests.swift
//  NYCSchoolsTests
//
//  Created by Gaurav Mehta on 16/04/19.
//  Copyright © 2019 Gaurav Mehta. All rights reserved.
//

import XCTest
import Foundation
import ObjectMapper

@testable import NYCSchools

class NSHighSchoolListTests: XCTestCase {
    var controller : NSHighSchoolListVC!
    
    override func setUp() {
        super.setUp()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        controller = storyboard.instantiateViewController(withIdentifier: "NSHighSchoolListVC") as? NSHighSchoolListVC
        let _ = controller.view
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testNumberOfRow(){
        _ = controller?.tableView(UITableView(), numberOfRowsInSection: 0) ?? 0
    }
    
    func testSchoolListMockJsonMethods(){
        var jsonObj = [String: AnyObject]()
        let testBundle = Bundle(for: NYCSchoolsTests.self)
        
        let path = testBundle.path(forResource: "NYCSchoolsTestsJson", ofType: "json")
        jsonObj = NSUtility.jsonForFileAtPath(path!)
        let schoolListModel = Mapper<NSListModel>().map(JSON: jsonObj)
        controller.handleOnSuceess(schoolListModel!, serviceIdentifier: ServiceIdentifiers.schoolListService)
        let error = NSError(domain: "", code: 0, userInfo: ["message" : "Object does not exist"])
        controller.handleOnFailure(error, serviceIdentifier: ServiceIdentifiers.schoolListService)
    }
    
    func testHandleOnFailure(){
        let error = NSError(domain: "", code: 0, userInfo: ["message" : "Object does not exist"])
        XCTAssertNotNil(controller.handleOnFailure(error, serviceIdentifier: ServiceIdentifiers.schoolListService), "testHandleOnFailure not called")
    }
}

