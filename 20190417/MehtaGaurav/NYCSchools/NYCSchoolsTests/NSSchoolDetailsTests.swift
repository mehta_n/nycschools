//
//  NSDetailsTests.swift
//  NYCSchoolsTests
//
//  Created by Gaurav Mehta on 16/04/19.
//  Copyright © 2019 Gaurav Mehta. All rights reserved.
//


import XCTest
import Foundation
import ObjectMapper

@testable import NYCSchools

class NSSchoolDetailsTests: XCTestCase {
    var controller : NSSchoolDetailsVC!
    
    override func setUp() {
        super.setUp()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        controller = storyboard.instantiateViewController(withIdentifier: "NSSchoolDetailsVC") as? NSSchoolDetailsVC
        let _ = controller.view
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testViewDidLoad(){
        _ = controller.viewDidLoad()
    }
    
    func testViewWillAppear(){
        _ = controller.viewWillAppear(true)
    }
    
    func testbtnBackMainAction(){
        _ = controller.btnBackMainAction(UIButton())
    }
    
    func testDisplayData(){
        _ = controller.displayData()
    }
    
    func testSchoolDetailsMockJsonMethods(){
        var jsonObj = [String: AnyObject]()
        let testBundle = Bundle(for: NYCSchoolsTests.self)
        
        let path = testBundle.path(forResource: "NYCDetailsTestsJson", ofType: "json")
        jsonObj = NSUtility.jsonForFileAtPath(path!)
        let schoolDetailsModel = Mapper<NSListModel>().map(JSON: jsonObj)
        controller.handleOnSuceess(schoolDetailsModel!, serviceIdentifier: ServiceIdentifiers.schoolDetailService)
        let error = NSError(domain: "", code: 0, userInfo: ["message" : "Object does not exist"])
        controller.handleOnFailure(error, serviceIdentifier: ServiceIdentifiers.schoolDetailService)
    }
    
    func testHandleOnFailure(){
        let error = NSError(domain: "", code: 0, userInfo: ["message" : "Object does not exist"])
        XCTAssertNotNil(controller.handleOnFailure(error, serviceIdentifier: ServiceIdentifiers.schoolDetailService), "testHandleOnFailure not called")
    }
}
