//
//  NSListTvCell.swift
//  NYCSchools
//
//  Created by Gaurav Mehta on 16/04/19.
//  Copyright © 2019 Gaurav Mehta. All rights reserved.
//

import UIKit

class NSListTvCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    
    //Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file.
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    //Sets the selected state of the cell, optionally animating the transition between states.
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell(item: NSSchoolList){
        self.lblName.text       = (item.school_name ?? "")
        self.lblMobile.text     = (item.phone_number ?? "")
        self.lblEmail.text      = (item.school_email ?? "")
        
        let add1 = (item.primary_address_line_1 ?? "") + " "
        let add2 = (item.state_code ?? "") + " - " + (item.zip ?? "")
        self.lblAddress.text    =  add1 + add2
    }
}
