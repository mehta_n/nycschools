//
//  NSDetailModel.swift
//  NYCSchools
//
//  Created by Gaurav Mehta on 16/04/19.
//  Copyright © 2019 Gaurav Mehta. All rights reserved.
//

import ObjectMapper

class NSGenericModel : Mappable {
    var response : Bool?
    var code : String?
    var message : String?
    
    required init?(map: Map) {
        // Mappable
    }
    
    func mapping(map: Map) {
        response <- map["response"]
        code <- map["code"]
        message <- map["message"]
    }
}

