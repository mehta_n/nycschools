//
//  NSDetailModel.swift
//  NYCSchools
//
//  Created by Gaurav Mehta on 16/04/19.
//  Copyright © 2019 Gaurav Mehta. All rights reserved.
//

import ObjectMapper

class NSListModel : Mappable {
    var response : Bool?
    var code : String?
    var message : String?
    var schoolList : [NSSchoolList]?

    required init?(map: Map) {
        // Mappable
    }
    
    func mapping(map: Map) {
        schoolList <- map["schoolList"]
    }
}

class NSSchoolList : Mappable {
    var academicopportunities1 : String?
    var academicopportunities2 : String?
    var admissionspriority11 : String?
    var admissionspriority21 : String?
    var admissionspriority31 : String?
    var attendance_rate : String?
    var bbl : String?
    var bin : String?
    var boro : String?
    var borough : String?
    var building_code : String?
    var bus : String?
    var census_tract : String?
    var city : String?
    var code1 : String?
    var community_board : String?
    var council_district : String?
    var dbn : String?
    var directions1 : String?
    var ell_programs : String?
    var extracurricular_activities : String?
    var fax_number : String?
    var finalgrades : String?
    var grade9geapplicants1 : String?
    var grade9geapplicantsperseat1 : String?
    var grade9gefilledflag1 : String?
    var grade9swdapplicants1 : String?
    var grade9swdapplicantsperseat1 : String?
    var grade9swdfilledflag1 : String?
    var grades2018 : String?
    var interest1 : String?
    var latitude : String?
    var location : String?
    var longitude : String?
    var method1 : String?
    var neighborhood : String?
    var nta : String?
    var offer_rate1 : String?
    var overview_paragraph : String?
    var pct_stu_enough_variety : String?
    var pct_stu_safe : String?
    var phone_number : String?
    var primary_address_line_1 : String?
    var program1 : String?
    var requirement1_1 : String?
    var requirement2_1 : String?
    var requirement3_1 : String?
    var requirement4_1 : String?
    var requirement5_1 : String?
    var school_10th_seats : String?
    var school_accessibility_description : String?
    var school_email : String?
    var school_name : String?
    var school_sports : String?
    var seats101 : String?
    var seats9ge1 : String?
    var seats9swd1 : String?
    var state_code : String?
    var subway : String?
    var total_students : String?
    var website : String?
    var zip : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        academicopportunities1 <- map["academicopportunities1"]
        academicopportunities2 <- map["academicopportunities2"]
        admissionspriority11 <- map["admissionspriority11"]
        admissionspriority21 <- map["admissionspriority21"]
        admissionspriority31 <- map["admissionspriority31"]
        attendance_rate <- map["attendance_rate"]
        bbl <- map["bbl"]
        bin <- map["bin"]
        boro <- map["boro"]
        borough <- map["borough"]
        building_code <- map["building_code"]
        bus <- map["bus"]
        census_tract <- map["census_tract"]
        city <- map["city"]
        code1 <- map["code1"]
        community_board <- map["community_board"]
        council_district <- map["council_district"]
        dbn <- map["dbn"]
        directions1 <- map["directions1"]
        ell_programs <- map["ell_programs"]
        extracurricular_activities <- map["extracurricular_activities"]
        fax_number <- map["fax_number"]
        finalgrades <- map["finalgrades"]
        grade9geapplicants1 <- map["grade9geapplicants1"]
        grade9geapplicantsperseat1 <- map["grade9geapplicantsperseat1"]
        grade9gefilledflag1 <- map["grade9gefilledflag1"]
        grade9swdapplicants1 <- map["grade9swdapplicants1"]
        grade9swdapplicantsperseat1 <- map["grade9swdapplicantsperseat1"]
        grade9swdfilledflag1 <- map["grade9swdfilledflag1"]
        grades2018 <- map["grades2018"]
        interest1 <- map["interest1"]
        latitude <- map["latitude"]
        location <- map["location"]
        longitude <- map["longitude"]
        method1 <- map["method1"]
        neighborhood <- map["neighborhood"]
        nta <- map["nta"]
        offer_rate1 <- map["offer_rate1"]
        overview_paragraph <- map["overview_paragraph"]
        pct_stu_enough_variety <- map["pct_stu_enough_variety"]
        pct_stu_safe <- map["pct_stu_safe"]
        phone_number <- map["phone_number"]
        primary_address_line_1 <- map["primary_address_line_1"]
        program1 <- map["program1"]
        requirement1_1 <- map["requirement1_1"]
        requirement2_1 <- map["requirement2_1"]
        requirement3_1 <- map["requirement3_1"]
        requirement4_1 <- map["requirement4_1"]
        requirement5_1 <- map["requirement5_1"]
        school_10th_seats <- map["school_10th_seats"]
        school_accessibility_description <- map["school_accessibility_description"]
        school_email <- map["school_email"]
        school_name <- map["school_name"]
        school_sports <- map["school_sports"]
        seats101 <- map["seats101"]
        seats9ge1 <- map["seats9ge1"]
        seats9swd1 <- map["seats9swd1"]
        state_code <- map["state_code"]
        subway <- map["subway"]
        total_students <- map["total_students"]
        website <- map["website"]
        zip <- map["zip"]
    }
}
