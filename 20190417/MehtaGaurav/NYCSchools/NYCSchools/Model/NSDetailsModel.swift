//
//  NSDetailModel.swift
//  NYCSchools
//
//  Created by Gaurav Mehta on 16/04/19.
//  Copyright © 2019 Gaurav Mehta. All rights reserved.
//

import ObjectMapper

class NSDetailsModel : Mappable {
    
    var response : Bool?
    var code : String?
    var message : String?
    var scoreList : [NSScoreList]?
    
    required init?(map: Map) {
        // Mappable
    }
    
    func mapping(map: Map) {
        response <- map["response"]
        code <- map["code"]
        message <- map["message"]
        scoreList <- map["scoreList"]
    }
}

class NSScoreList : Mappable {
    var dbn : String?
    var num_of_sat_test_takers : String?
    var sat_critical_reading_avg_score : String?
    var sat_math_avg_score : String?
    var sat_writing_avg_score : String?
    var school_name : String?
    
    required init?(map: Map) {
        // Mappable
    }
    
    func mapping(map: Map) {
        dbn <- map["dbn"]
        num_of_sat_test_takers <- map["num_of_sat_test_takers"]
        sat_critical_reading_avg_score <- map["sat_critical_reading_avg_score"]
        sat_math_avg_score <- map["sat_math_avg_score"]
        sat_writing_avg_score <- map["sat_writing_avg_score"]
        school_name <- map["school_name"]
    }    
}
