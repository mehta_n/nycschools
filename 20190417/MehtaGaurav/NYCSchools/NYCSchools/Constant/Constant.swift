////
////  Constant.swift
////  Constant
////
////  Created by Gaurav Mehta on 4/22/18.
////  Copyright © 2018 Gaurav Mehta. All rights reserved.
////

import UIKit
import Foundation

let AppName     = "NYC Schools"
let PrimaryColor    = UIColor(red: 19.0/255.0, green: 155.0/255.0, blue: 255.0/255.0, alpha: 1.0)
let mainSB      = UIStoryboard(name: "Main", bundle:nil)

let ListTvCell      = "NSListTvCell"
