//
//  NSListVC.swift
//  NYCSchools
//
//  Created by Gaurav Mehta on 16/04/19.
//  Copyright © 2019 Gaurav Mehta. All rights reserved.
//

import UIKit

class NSHighSchoolListVC: UIViewController {

    // MARK:- All IBOutlet and property
    @IBOutlet weak var viewNavigation : UIView!
    @IBOutlet weak var tblView: UITableView!
    var model : NSListModel?
    var intRow      = 0
    var isLoadMore  = true
    
    // MARK:- View Controller Cycle
    override func viewDidLoad() {
        self.setUpVC()
        super.viewDidLoad()
        self.callWebService()
    }

    // MARK:- Setup View
    func setUpVC(){
        self.tblView.register(UINib(nibName: ListTvCell, bundle: nil), forCellReuseIdentifier: ListTvCell)
    }
    
    // MARK:- Web Service Call
    func callWebService(){
        NSProgressView.shared.showProgressView(view)
        NSWebServices.sharedInstance.schoolList(completion: { (response, serviceIdentifier) in
            self.handleOnSuceess(response, serviceIdentifier: serviceIdentifier)
        }, onError: { (error, serviceIdentifier) in
            self.handleOnFailure(error, serviceIdentifier: serviceIdentifier)
        })
    }
    
    //Load More Data Once Reach End in TableView Cell
    func loadMoreData(){
        if (self.model?.schoolList?.count ?? 0) <= self.intRow{
            self.isLoadMore = false
        }else{
            self.intRow += 20
            self.tblView.reloadData()
        }
    }
}

// MARK:- UITableView Delegate and Data Source
extension NSHighSchoolListVC: UITableViewDelegate, UITableViewDataSource{
    
    //Tells the data source to return the number of rows in a given section of a table view.
    //Parameters
    //-tableView
    //The table-view object requesting this information.
    //-section
    //An index number identifying a section in tableView.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.intRow
    }
    
    //Asks the data source for a cell to insert in a particular location of the table view.
    //Parameters
    //tableView
    //A table-view object requesting the cell.
    //indexPath
    //An index path locating a row in tableView.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ListTvCell, for: indexPath) as! NSListTvCell
        cell.configureCell(item: self.model!.schoolList![indexPath.row])
        if (self.intRow - 1) == indexPath.row{
            self.loadMoreData()
        }
        return cell
    }
    
    //Summary
    //Tells the delegate that the specified row is now selected.
    //Parameters
    //tableView
    //A table-view object informing the delegate about the new row selection.
    //indexPath
    //An index path locating the new selected row in tableView.
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let VC = mainSB.instantiateViewController(withIdentifier: "NSSchoolDetailsVC") as! NSSchoolDetailsVC
        VC.schoolList    = self.model?.schoolList![indexPath.row]
        self.navigationController?.pushViewController(VC, animated: true)
    }
}

// MARK:- Response Handler
extension NSHighSchoolListVC {
    
    func handleOnSuceess(_ response:AnyObject,serviceIdentifier:String) {
        NSProgressView.shared.hideProgressView()

        switch serviceIdentifier {
        case ServiceIdentifiers.schoolListService:
            self.model = response as? NSListModel
            self.loadMoreData()
            break
        default:
            break
        }
    }
    
    func handleOnFailure(_ error:Error,serviceIdentifier:String) {
        NSProgressView.shared.hideProgressView()
    }
}
