//
//  NSSchoolDetailsVC.swift
//  NYCSchools
//
//  Created by Gaurav Mehta on 16/04/19.
//  Copyright © 2019 Gaurav Mehta. All rights reserved.
//

import UIKit

class NSSchoolDetailsVC: UIViewController {
    
    // MARK:- All IBOutlet and property
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var viewSub: UIView!
    @IBOutlet weak var lblTaskers: UILabel!
    @IBOutlet weak var lblMath: UILabel!
    @IBOutlet weak var lblWriting: UILabel!
    @IBOutlet weak var lblReading: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblNoData: UILabel!
    
    var detailsModel : NSDetailsModel?
    var schoolList : NSSchoolList?
    var scoreList : NSScoreList?
    
    // MARK:- View Controller Cycle
    //Called after the controller's view is loaded into memory.
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewSub.isHidden   = true
        self.lblNoData.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.callWebService()
    }

    // MARK:- Back Button Action
    @IBAction func btnBackMainAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    // MARK:- Web Service Call
    func callWebService(){
        NSProgressView.shared.showProgressView(view)
        NSWebServices.sharedInstance.schoolDetails(completion: { (response, serviceIdentifier) in
            self.handleOnSuceess(response, serviceIdentifier: serviceIdentifier)
        }, onError: { (error, serviceIdentifier) in
            self.handleOnFailure(error, serviceIdentifier: serviceIdentifier)
        })
    }
}


// MARK:- Response Handler
extension NSSchoolDetailsVC {
    func handleOnSuceess(_ response:AnyObject,serviceIdentifier:String) {
        NSProgressView.shared.hideProgressView()
        switch serviceIdentifier {
        case ServiceIdentifiers.schoolDetailService:
            self.detailsModel = response as? NSDetailsModel
            self.displayData()
            break
        default:
            break
        }
    }
    
    func displayData(){
        let filterArr = self.detailsModel?.scoreList?.filter{$0.dbn == (self.schoolList?.dbn ?? "")}
        
        if (filterArr?.count ?? 0) > 0{
            self.scoreList = filterArr![0]
            self.lblNoData.isHidden = true
            self.viewSub.isHidden   = false
        }else{
            self.lblNoData.isHidden = false
            self.viewSub.isHidden   = true
        }
        self.lblTitle.text       = (self.scoreList?.school_name ?? "")
        self.lblTaskers.text    = (self.scoreList?.num_of_sat_test_takers ?? "")
        self.lblMath.text       = (self.scoreList?.sat_math_avg_score ?? "")
        self.lblWriting.text    = (self.scoreList?.sat_writing_avg_score ?? "")
        self.lblReading.text    = (self.scoreList?.sat_critical_reading_avg_score ?? "")
    }
    
    func handleOnFailure(_ error:Error,serviceIdentifier:String) {
        NSProgressView.shared.hideProgressView()
    }
}
