import UIKit
import ObjectMapper

struct ServiceIdentifiers {
    static let baseServiceURL   = "https://data.cityofnewyork.us/resource/" //Main URL
    static let schoolListService             = "s3k6-pzi2.json"
    static let schoolDetailService           = "f9bf-2cp4.json"
}

class NSWebServices: NSObject {
    let kGetMethod      = "GET"
    var serviceQueue    = [NSBaseService]()
    static let sharedInstance = NSWebServices()
    
    fileprivate override init() {
        //init
    }
    
    // MARK:- School List Service
    func schoolList(completion: @escaping (_ response:AnyObject,_ serviceIdentifier:String)->(),
                        onError: @escaping (_ error:Error,_ serviceIdentifier:String)->()){
        let localSerIdentifier = ServiceIdentifiers.schoolListService
        
        let service = NSBaseService()
        service.serviceIdentifier = localSerIdentifier
        service.makeServiceRequestWithUrl(url: ServiceIdentifiers.baseServiceURL + localSerIdentifier, requestType: kGetMethod, parameters: [:], headers: [:], body:[:] , serviceIdentifier: localSerIdentifier, success: { (response) in
            let responsse = "{\"schoolList\":\(response)}"
            let model = Mapper<NSListModel>().map(JSONString: responsse)
            if model != nil{
                completion(model!, service.serviceIdentifier)
            }else{
                onError(NSError(domain: "Method Not Allowed", code: 405, userInfo: nil),service.serviceIdentifier)
            }
        }) { (errorResponse) in
            onError(errorResponse,service.serviceIdentifier)
        }
    }
    
    // MARK:- School Detail Service
    func schoolDetails(completion: @escaping (_ response:AnyObject,_ serviceIdentifier:String)->(),
              onError: @escaping (_ error:Error,_ serviceIdentifier:String)->()){
        let localSerIdentifier = ServiceIdentifiers.schoolDetailService
        
        let service = NSBaseService()
        service.serviceIdentifier = localSerIdentifier
        service.makeServiceRequestWithUrl(url: ServiceIdentifiers.baseServiceURL + localSerIdentifier, requestType: kGetMethod, parameters: [:], headers: [:], body:[:] , serviceIdentifier: localSerIdentifier, success: { (response) in
            let responsse = "{\"scoreList\":\(response)}"
            let model = Mapper<NSDetailsModel>().map(JSONString: responsse)
            if model != nil{
                completion(model!, service.serviceIdentifier)
            }else{
                onError(NSError(domain: "Method Not Allowed", code: 405, userInfo: nil),service.serviceIdentifier)
            }
        }) { (errorResponse) in
            onError(errorResponse,service.serviceIdentifier)
        }
    }
}
