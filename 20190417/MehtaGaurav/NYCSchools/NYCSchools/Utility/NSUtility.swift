import UIKit
import Foundation

class NSUtility : NSObject{

    class func getUnwrappedValue<T>(_ object: T?) -> T? {
        if let unwrappedObj = object {
            return unwrappedObj
        }
        return nil
    }
    
    open class func jsonForFileAtPath(_ filePath:String) -> [String: AnyObject]{
        var jsonObj = [String: AnyObject]()
        do {
            let text = try NSString(contentsOfFile: filePath,
                                    encoding: String.Encoding.ascii.rawValue)
            if text is NSString {
                let data = text.data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)!
                do {
                    jsonObj = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
            }
        } catch {
            // handle error
            print("Catch block executed")
        }
        return jsonObj
    }
}
