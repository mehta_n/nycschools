////
////  NSAlertViewController.swift
////  NSAlertViewController
////
////  Created by Gaurav Mehta on 4/22/18.
////  Copyright © 2018 Gaurav Mehta. All rights reserved.
////

import UIKit

class NSAlertViewController: UIAlertController {
    
    fileprivate lazy var alertWindow: UIWindow = {
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = NSAlertBackgroundViewController()
        window.backgroundColor = UIColor.clear
        return window
    }()
    
    func show(animated flag: Bool = true, completion: (() -> Void)? = nil) {
        let alertVisisble = isalertViewVisible()
        
        if !alertVisisble {
            if let rootViewController = alertWindow.rootViewController {
                alertWindow.makeKeyAndVisible()
                rootViewController.present(self, animated: flag, completion: completion)
            }
        }
    }
    
    func anyAlertWindowIsActive() -> Bool {
        var isKeyActive = false
        for (_,element) in UIApplication.shared.windows.enumerated() {
            if element.rootViewController is NSAlertBackgroundViewController {
                if element.isKeyWindow == true {
                    isKeyActive = true
                }
            }
        }
        return isKeyActive
    }

    func isalertViewVisible() -> Bool {
        if anyAlertWindowIsActive() == true {
            for (_,element) in UIApplication.shared.windows.enumerated() {
                if element.rootViewController is NSAlertBackgroundViewController {
                    return true
                }
            }
        }else {
            return false
        }
        return false
    }
}

// In the case of view controller-based status bar style, make sure we use the same style for our view controller
private class NSAlertBackgroundViewController: UIViewController {
    
    fileprivate override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIApplication.shared.statusBarStyle
    }
    
    fileprivate override var prefersStatusBarHidden : Bool {
        return UIApplication.shared.isStatusBarHidden
    }
}

